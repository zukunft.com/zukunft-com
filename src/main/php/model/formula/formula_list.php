<?php

/*

    formula_list.php - a simple list of formulas
    ----------------

    This file is part of zukunft.com - calc with words

    zukunft.com is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as
    published by the Free Software Foundation, either version 3 of
    the License, or (at your option) any later version.
    zukunft.com is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with zukunft.com. If not, see <http://www.gnu.org/licenses/gpl.html>.

    To contact the authors write to:
    Timon Zielonka <timon@zukunft.com>

    Copyright (c) 1995-2022 zukunft.com AG, Zurich
    Heang Lor <heang@zukunft.com>

    http://zukunft.com

*/

class formula_list
{

    public array $lst;           // the list of the loaded formula objects
    public user $usr;            // if 0 (not NULL) for standard formulas, otherwise for a user specific formulas

    // fields to select the formulas
    public ?word $wrd = null;            // show the formulas related to this word
    public ?phrase_list $phr_lst = null; // show the formulas related to this phrase list
    public ?array $ids = array();        // a list of formula ids to load all formulas at once

    // in memory only fields
    public ?string $back = null;         // the calling stack

    /**
     * always set the user because a formula list is always user specific
     * @param user $usr the user who requested to see the formulas
     */
    function __construct(user $usr)
    {
        $this->lst = array();
        $this->usr = $usr;
    }

    /**
     * fill the formula list based on a database records
     * @param array $db_rows is an array of an array with the database values
     * @return bool true if at least one formula has been loaded
     */
    private function rows_mapper(array $db_rows): bool
    {
        $result = false;
        if ($db_rows != null) {
            foreach ($db_rows as $db_row) {
                if (is_null($db_row[user_sandbox::FLD_EXCLUDED]) or $db_row[user_sandbox::FLD_EXCLUDED] == 0) {
                    if ($db_row[formula::FLD_ID] > 0) {
                        $frm = new formula($this->usr);
                        $frm->row_mapper($db_row);
                        if ($frm->name <> '') {
                            $name_wrd = new word($this->usr);
                            $name_wrd->name = $frm->name;
                            $name_wrd->load();
                            $frm->name_wrd = $name_wrd;
                        }
                        $this->lst[] = $frm;
                        $result = true;
                    }
                }
            }
        }
        return $result;
    }

    /*
     * load functions
     */

    /**
     * set the SQL query parameters to load a list of formulas
     * @param sql_db $db_con the db connection object as a function parameter for unit testing
     * @return sql_par the SQL statement, the name of the SQL statement and the parameter list
     */
    function load_sql(sql_db $db_con): sql_par
    {
        $qp = new sql_par(self::class);
        $db_con->set_type(DB_TYPE_FORMULA);
        $db_con->set_usr($this->usr->id);
        $db_con->set_name($qp->name); // assign incomplete name to force the usage of the user as a parameter
        $db_con->set_usr_fields(formula::FLD_NAMES_USR);
        $db_con->set_usr_num_fields(formula::FLD_NAMES_NUM_USR);
        return $qp;
    }

    /**
     * set the SQL query parameters to load a list of formulas by an array of formula ids
     * @param sql_db $db_con the db connection object as a function parameter for unit testing
     * @param array $frm_ids an array of formula ids which should be loaded
     * @return sql_par the SQL statement, the name of the SQL statement and the parameter list
     */
    function load_sql_by_frm_ids(sql_db $db_con, array $frm_ids): sql_par
    {
        $qp = $this->load_sql($db_con);
        if (count($frm_ids) > 0) {
            $qp->name .= 'frm_ids';
            $db_con->set_name($qp->name);
            $db_con->add_par_in_int($frm_ids);
            $qp->sql = $db_con->select_by_field(formula::FLD_ID);
        } else {
            $qp->name = '';
        }
        $qp->par = $db_con->get_par();
        return $qp;
    }

    /**
     * set the SQL query parameters to load a list of formulas linked to one of the phrases from the given list
     * @param sql_db $db_con the db connection object as a function parameter for unit testing
     * @param phrase $phr a phrase used to select the formulas
     * @return sql_par the SQL statement, the name of the SQL statement and the parameter list
     */
    function load_sql_by_phr(sql_db $db_con, phrase $phr): sql_par
    {
        $qp = $this->load_sql($db_con);
        if ($phr->id <> 0) {
            $qp->name .= 'phr';
            $db_con->set_name($qp->name);
            $db_con->set_join_fields(
                array(phrase::FLD_ID),
                DB_TYPE_FORMULA_LINK,
                formula::FLD_ID,
                formula::FLD_ID
            );
            $db_con->add_par(sql_db::PAR_INT, $phr->id, false, true);
            $qp->sql = $db_con->select_by_field(phrase::FLD_ID);
        } else {
            $qp->name = '';
        }
        $qp->par = $db_con->get_par();
        return $qp;
    }

    /**
     * set the SQL query parameters to load a list of formulas linked to one of the phrases from the given list
     * @param sql_db $db_con the db connection object as a function parameter for unit testing
     * @param phrase_list $phr_lst a phrase list used to select the formulas
     * @return sql_par the SQL statement, the name of the SQL statement and the parameter list
     */
    function load_sql_by_phr_lst(sql_db $db_con, phrase_list $phr_lst): sql_par
    {
        $qp = $this->load_sql($db_con);
        if ($phr_lst->count() > 0) {
            $qp->name .= 'phr_lst';
            $db_con->set_name($qp->name);
            $db_con->set_join_fields(
                array(phrase::FLD_ID),
                DB_TYPE_FORMULA_LINK,
                formula::FLD_ID,
                formula::FLD_ID
            );
            $db_con->add_par_in_int($phr_lst->id_lst(), false, true);
            $qp->sql = $db_con->select_by_field(phrase::FLD_ID);
        } else {
            $qp->name = '';
        }
        $qp->par = $db_con->get_par();
        return $qp;
    }

    /**
     * set the SQL query parameters to load a set of all formulas
     * @param sql_db $db_con the db connection object as a function parameter for unit testing
     * @param int $limit the number of formulas that should be loaded
     * @param int $page the offset
     * @return sql_par the SQL statement, the name of the SQL statement and the parameter list
     */
    function load_sql_all(sql_db $db_con, int $limit, int $page): sql_par
    {
        $qp = new sql_par(self::class);
        $db_con->set_type(DB_TYPE_FORMULA);
        $db_con->set_usr($this->usr->id);
        $db_con->set_all();
        $qp->name = formula_list::class . '_all';
        $db_con->set_name($qp->name);
        $db_con->set_usr_fields(formula::FLD_NAMES_USR);
        $db_con->set_usr_num_fields(formula::FLD_NAMES_NUM_USR);
        if ($limit > 0) {
            $db_con->set_order(formula::FLD_ID);
            $db_con->set_page_par($limit, $page);
            $qp->sql = $db_con->select_all();
        } else {
            $qp->name = '';
        }
        $qp->par = $db_con->get_par();
        return $qp;
    }

    /**
     * load a list of formulas
     * @param sql_par $qp the SQL statement, the unique name of the SQL statement and the parameter list
     * @return bool true if at least one formula has been loaded
     */
    private function load_int(sql_par $qp): bool
    {

        global $db_con;
        $result = false;

        // check the all minimal input parameters are set
        if ($qp->name == '') {
            log_err('The query name cannot be created to load a ' . self::class, self::class . '->load');
        } else {
            $db_lst = $db_con->get($qp);
            $result = $this->rows_mapper($db_lst);
        }
        return $result;
    }

    /**
     * load a list of formula links with the direct linked phrases related to the given formula id
     * @param array $frm_ids an array of formula ids which should be loaded
     * @return bool true if at least one word found
     */
    function load_by_frm_ids(array $frm_ids): bool
    {
        global $db_con;
        $qp = $this->load_sql_by_frm_ids($db_con, $frm_ids);
        return $this->load_int($qp);
    }

    /**
     * load a list of formulas with are linked to one of the gives phrases
     * @param phrase $phr a phrase used to select the formulas
     * @return bool true if at least one word found
     */
    function load_by_phr(phrase $phr): bool
    {
        global $db_con;
        $qp = $this->load_sql_by_phr($db_con, $phr);
        return $this->load_int($qp);
    }

    /**
     * load a list of formulas with are linked to one of the gives phrases
     * @param phrase_list $phr_lst a phrase list used to select the formulas
     * @return bool true if at least one word found
     */
    function load_by_phr_lst(phrase_list $phr_lst): bool
    {
        global $db_con;
        $qp = $this->load_sql_by_phr_lst($db_con, $phr_lst);
        return $this->load_int($qp);
    }

    /**
     * load a snap of all formulas
     * @param int $limit the number of formulas that should be loaded
     * @param int $page the offset
     * @return bool true if at least one word found
     */
    function load_all(int $limit, int $page): bool
    {
        global $db_con;
        $qp = $this->load_sql_all($db_con, $limit, $page);
        return $this->load_int($qp);
    }

    /*
     * display functions
     */

    /**
     * return the loaded formula names for debugging
     */
    function dsp_id(): string
    {
        $result = $this->name();
        if ($result <> '') {
            $result = '"' . $result . '"';
        }
        return $result;
    }

    function name(): string
    {
        return dsp_array($this->names());
    }

    /**
     * this function is called from dsp_id, so no other call is allowed
     */
    function names(): array
    {
        $result = array();
        if ($this->lst != null) {
            foreach ($this->lst as $frm) {
                $result[] = $frm->name;
            }
        }
        return $result;
    }

    /**
     * lists all formulas with results related to a word
     */
    function display($type = 'short'): string
    {
        log_debug('formula_list->display ' . $this->dsp_id());
        $result = '';

        // list all related formula results
        if ($this->lst != null) {
            usort($this->lst, array("formula", "cmp"));
            if ($this->lst != null) {
                foreach ($this->lst as $frm) {
                    // formatting should be moved
                    //$resolved_text = str_replace('"','&quot;', $frm->usr_text);
                    //$resolved_text = str_replace('"','&quot;', $frm->dsp_text($this->back));
                    $frm_dsp = $frm->dsp_obj();
                    $formula_value = '';
                    if ($frm->name_wrd != null) {
                        $formula_value = $frm_dsp->dsp_result($frm->name_wrd, $this->back);
                    }
                    // if the formula value is empty use the id to be able to select the formula
                    if ($formula_value == '') {
                        $result .= $frm_dsp->id;
                    } else {
                        $result .= ' value ' . $formula_value;
                    }
                    $result .= ' ' . $frm_dsp->name_linked($this->back);
                    if ($type == 'short') {
                        $result .= ' ' . $frm_dsp->btn_del($this->back);
                        $result .= ', ';
                    } else {
                        $result .= ' (' . $frm_dsp->dsp_text($this->back) . ')';
                        $result .= ' ' . $frm_dsp->btn_del($this->back);
                        $result .= ' <br> ';
                    }
                }
            }
        }

        log_debug("formula_list->display ... done (" . $result . ")");
        return $result;
    }

    /**
     * @return int the number of suggested calculation blocks to update all formulas
     */
    function calc_blocks(sql_db $db_con, int $total_formulas = 0): int
    {
        if ($total_formulas == 0) {
            $total_formulas = $db_con->count(DB_TYPE_FORMULA);
        }
        $avg_calc_time = cfg_get(CFG_AVG_CALC_TIME, $db_con);
        $total_expected_time = $total_formulas * $avg_calc_time;
        return max(1, round($total_expected_time / (UI_MIN_RESPONSE_TIME * 1000)));
    }

}
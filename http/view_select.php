<?php

/*

  view_select.php - define how a word should be displayed
  ---------------
  
  This file is part of zukunft.com - calc with words

  zukunft.com is free software: you can redistribute it and/or modify it
  under the terms of the GNU General Public License as
  published by the Free Software Foundation, either version 3 of
  the License, or (at your option) any later version.
  zukunft.com is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with zukunft.com. If not, see <http://www.gnu.org/licenses/gpl.html>.
  
  To contact the authors write to:
  Timon Zielonka <timon@zukunft.com>
  
  Copyright (c) 1995-2022 zukunft.com AG, Zurich
  Heang Lor <heang@zukunft.com>
  
  http://zukunft.com
  
*/

// standard zukunft header for callable php files to allow debugging and lib loading
$debug = $_GET['debug'] ?? 0;
include_once '../src/main/php/zu_lib.php';

// open database
$db_con = prg_start("view_select");

$result = ''; // reset the html code var
$msg = ''; // to collect all messages that should be shown to the user immediately

// load the session user parameters
$usr = new user;
$result .= $usr->get();

// check if the user is permitted (e.g. to exclude crawlers from doing stupid stuff)
if ($usr->id > 0) {

    load_usr_data();

    // in view edit views the view cannot be changed
    $dsp = new view_dsp($usr);
    //$dsp->id = cl(SQL_VIEW_FORMULA_EXPLAIN);
    $back = $_GET['back']; // the original calling page that should be shown after the change if finished
    $result .= $dsp->dsp_navbar_no_view($back);
    $view_id = 0;
    $word_id = $back;

    // get the view id used utils now and the word id
    if (isset($_GET['id'])) {
        $view_id = $_GET['id'];
    }
    if (isset($_GET['word'])) {
        $word_id = $_GET['word'];
    }

    // show the word name
    $wrd = new word($usr);
    if ($word_id > 0) {
        $wrd->id = $word_id;
        $wrd->load();
        $result .= dsp_text_h2('Select the display format for "' . $wrd->name . '"');
    } else {
        $result .= dsp_text_h2('The word is missing for which the display format should be changed. If you can explain how to reproduce this error message, please report the steps on https://github.com/zukunft/zukunft.com/issues.');
    }

    // allow to change to type
    $dsp = new view($usr);
    $dsp->id = $view_id;
    $result .= $dsp->selector_page($word_id, $back);

    // show the changes
    $result .= $wrd->dsp_log_view($back);
}

echo $result;

prg_end($db_con);
